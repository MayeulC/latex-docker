FROM ubuntu:oracular
MAINTAINER Mayeul Cantan <oss+latex-docker@mayeul.net>
ENV DEBIAN_FRONTEND noninteractive

 RUN apt-get update -q && apt-get install -qy \
    texlive-full \
    python3-pygments gnuplot \
    make git jq cloc inkscape pdftk-java \
    graphviz fonts-dejavu \
    && rm -rf /var/lib/apt/lists/* && \
    luaotfload-tool --update

#  ttf-freefont ttf-liberation ttf-linux-libertine \
    
WORKDIR /data
VOLUME ["/data"]
